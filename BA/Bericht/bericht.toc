\contentsline {chapter}{\numberline {1}Theoretical background}{1}{chapter.1}
\contentsline {section}{\numberline {1.1}Introduction}{1}{section.1.1}
\contentsline {subsection}{\numberline {1.1.1}Coordinate system}{2}{subsection.1.1.1}
\contentsline {subsection}{\numberline {1.1.2}Classical cyclotron}{3}{subsection.1.1.2}
\contentsline {subsection}{\numberline {1.1.3}Isochroncyclotron}{3}{subsection.1.1.3}
\contentsline {subsection}{\numberline {1.1.4}$\sigma $ - Matrix}{5}{subsection.1.1.4}
\contentsline {subsection}{\numberline {1.1.5}Tracking methods}{5}{subsection.1.1.5}
\contentsline {section}{\numberline {1.2}Modern map theory}{6}{section.1.2}
\contentsline {subsection}{\numberline {1.2.1}On the Hamilton formalism and the transfer map}{6}{subsection.1.2.1}
\contentsline {section}{\numberline {1.3}Linear transfer maps}{8}{section.1.3}
\contentsline {subsection}{\numberline {1.3.1}Equations of motion and a simplified Hamiltonian}{8}{subsection.1.3.1}
\contentsline {subsection}{\numberline {1.3.2}Transfer matrices}{11}{subsection.1.3.2}
\contentsline {section}{\numberline {1.4}OPAL tracking}{13}{section.1.4}
\contentsline {subsection}{\numberline {1.4.1}Particle-particle method}{13}{subsection.1.4.1}
\contentsline {subsection}{\numberline {1.4.2}Particle-mesh method}{13}{subsection.1.4.2}
\contentsline {subsection}{\numberline {1.4.3}Bunch tracking in OPAL}{13}{subsection.1.4.3}
\contentsline {section}{\numberline {1.5}Matched distributions}{14}{section.1.5}
\contentsline {subsection}{\numberline {1.5.1}Computation of a matched distribution}{15}{subsection.1.5.1}
\contentsline {chapter}{\numberline {2}Simulation and Results}{17}{chapter.2}
\contentsline {section}{\numberline {2.1}High intensity cyclotrons at PSI}{17}{section.2.1}
\contentsline {subsection}{\numberline {2.1.1}Injector II}{18}{subsection.2.1.1}
\contentsline {subsection}{\numberline {2.1.2}Ring}{19}{subsection.2.1.2}
\contentsline {section}{\numberline {2.2}Matched distributions}{20}{section.2.2}
\contentsline {subsection}{\numberline {2.2.1}Injector II}{20}{subsection.2.2.1}
\contentsline {subsection}{\numberline {2.2.2}Ring}{22}{subsection.2.2.2}
\contentsline {section}{\numberline {2.3}OPAL tracking}{23}{section.2.3}
\contentsline {subsection}{\numberline {2.3.1}Injector II}{23}{subsection.2.3.1}
\contentsline {subsection}{\numberline {2.3.2}Ring}{27}{subsection.2.3.2}
\contentsline {chapter}{\numberline {3}Analysis and Critical Assessment}{31}{chapter.3}
\contentsline {section}{\numberline {3.1}Analysis of the data}{31}{section.3.1}
\contentsline {subsection}{\numberline {3.1.1}Initial beam sizes}{31}{subsection.3.1.1}
\contentsline {subsection}{\numberline {3.1.2}Matching point}{32}{subsection.3.1.2}
\contentsline {section}{\numberline {3.2}Conclusion and Outlook}{35}{section.3.2}
\contentsline {subsection}{\numberline {3.2.1}Simulation results}{35}{subsection.3.2.1}
\contentsline {subsection}{\numberline {3.2.2}On the matched distributions}{35}{subsection.3.2.2}
\contentsline {section}{\numberline {3.3}Acknowledgements}{37}{section.3.3}
