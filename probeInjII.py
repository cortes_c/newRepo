#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jul  2 10:38:35 2018

@author: cristopher
"""
import numpy as np
import matplotlib.pyplot as plt

    
data = np.loadtxt("/home/cristopher/Documents/PSI/newRepo/InjIIprobe/PropertiesForEnergy2MeV.dat",
                      skiprows = 1, unpack =True)
    
r = data[0]
p = data[1]
h = data[2]
n = data[3]
ds = data[4]
s=[]
sume = 0;
for i in ds:
    sume = sume + i;
    s.append(sume)   
    
def plotBeamSize():
    
    sigmas = np.loadtxt("InjIIprobe/SigmaPerAngleForEnergy2_2MeV.dat",unpack = True)
    
    sigmax1 = []
    sigmay1 = []
    sigmaz1 = []
    
    l = int(len(sigmas[0])/6)
    
    for i in range(l):
        
        sigmax1.append(np.sqrt(np.abs(sigmas[0][i*6])))
        sigmay1.append(np.sqrt(np.abs(sigmas[2][i*6+2])))
        sigmaz1.append(np.sqrt(np.abs(sigmas[4][i*6+4])))    
    
    S = s[-len(sigmax1):]
        
    sigmax1 = np.array(sigmax1)
    sigmay1 = np.array(sigmay1)
    sigmaz1 = np.array(sigmaz1)
    
    slong = []
    sigx = []
    sigy = []
    sigz = []
    
    for i in range(1):
        
        slong = np.append(slong,S+i*(S[len(S)-1]))
        sigx = np.append(sigx,sigmax1)
        sigy = np.append(sigy,sigmay1)
        sigz =np.append(sigz,sigmaz1)
    
    plt.plot(slong,sigx,label = r"$\sigma_x$")
    plt.plot(slong,sigy,label = r"$\sigma_z$")
    plt.plot(slong,sigz,label = r"$\sigma_l$")
    plt.xlabel("s [m]")
    plt.ylabel(r"$\sigma$ [mm]")
    plt.legend(loc=0)
        
plotBeamSize()