#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Jun 23 21:34:42 2018

@author: cristopher
"""
"use %matplotlib auto :)"
import numpy as np
import matplotlib.pyplot as plt

Energy = 72

#data = np.loadtxt("data{}MeV/PropertiesForEnergy{}MeV.dat".format(Energy,Energy),skiprows = 1,unpack=True)
#referenz = np.loadtxt("data{}MeV/Matches{}MeV.txt".format(Energy,Energy),unpack=True)

data = np.loadtxt("data{}MeVlow/PropertiesForEnergy{}MeV.dat".format(Energy,Energy),skiprows = 1,unpack=True)
referenz = np.loadtxt("data{}MeVlow/Matches{}MeV.txt".format(Energy,Energy),unpack=True)

I = referenz[1][0]*1000

radius = data[0]    
h = data[2]
n = data[3]
ds = data[4]
s = []
f = 50.65*10**6 #Hz
c = 299792458 #m/s
e = 1.602177*10**-19 #[C]
ep0 = 8.854187817*10**-12
m = 254.4*10**9 #[GeV/m**2]
N_h = 6

sume = 0;
for i in ds:
    sume = sume + i;
    s.append(sume)
    
def gamma(r):
    
    a = c/f
    gamma = 1/np.sqrt(1-(r/a)**2)
    
    return gamma

def beta(gamma):
    
    beta = np.sqrt(1-(1/gamma**2))
    return beta

def calculate_K(sigmax,sigmay,sigmaz,i):
    
    M = m*e
    Q = i/f
    N = Q/e
    q = Q/N
    g = gamma(radius)
    g = g[:-876]
    b = beta(g)
    lamda = 2*np.pi*c/(f*N_h)
    K3 = (3*q*I*lamda) /(20*np.sqrt(5)*np.pi*ep0*c*M*(b*g)**2)
    form = np.sqrt(sigmax*sigmay)/(3*g*sigmaz)
    
    Kx = K3*(1-form) / ((sigmax+sigmaz)*sigmax*sigmay)
    Ky = K3*(1-form) / ((sigmax+sigmaz)*sigmay*sigmaz)
    Kz = K3*form / (sigmax*sigmay*sigmaz)
    
    return Kx,Ky,Kz

def calculate_D(Kx,Ky,Kz):
    
    R = np.array(radius[:-876])
    N = np.array(n[:-876])
    H = np.array(h[:-876])
    g = gamma(R) 
    kx = 1 - N*H**2
    ky = N*H**2
    omxz2 = (Kx+Kz-kx)/2
    omsc2 = (omxz2-Kx)**2 - (H*g*Kz)**2
    
    Omp2 = omxz2 + omsc2
    Omm2 = omxz2 - omsc2
    
    Omp = np.sqrt(np.abs(Omp2))
    Omm = np.sqrt(np.abs(Omm2))
    
    omz = np.sqrt(np.abs(Ky - ky))
    
    return Omp, Omm, omz
    
def plotingOmega():
    
    sigmas = np.loadtxt("data{}MeV/SigmaPerAngleForEnergy2_{}MeV.dat".format(Energy,Energy),unpack=True)
    
    sigmax1 = []
    sigmay1 = []
    sigmaz1 = []
    
    l = int(len(sigmas[0])/6)
    
    for i in range(l):
        
        sigmax1.append(np.sqrt(np.abs(sigmas[0][i*6]))*10**-3)
        sigmay1.append(np.sqrt(np.abs(sigmas[2][i*6+2]))*10**-3)
        sigmaz1.append(np.sqrt(np.abs(sigmas[4][i*6+4]))*10**-3)
        
    sigmax1 = np.array(sigmax1)
    sigmay1 = np.array(sigmay1)
    sigmaz1 = np.array(sigmaz1)
      
    S = s[:-876]
    
    slong = []
    sigx = []
    sigy = []
    sigz = []
    
    for i in range(8):
        
        slong = np.append(slong,S+i*(S[len(S)-1]))
        sigx = np.append(sigx,sigmax1*1000)
        sigy = np.append(sigy,sigmay1*1000)
        sigz =np.append(sigz,sigmaz1*1000)
        
    plt.figure(1)
            
    plt.plot(slong,sigx,label =r"$\sigma_{x}$",color="orange")
    plt.plot(slong,sigy,label= r"$\sigma_{z}$",color="blue")
    plt.plot(slong,sigz,label=r"$\sigma_{l}$",color="purple")
        
    plt.title(r"Beam size for one turn ({}MeV , {}mA)".format(Energy,I),fontsize = 16)
    plt.xlabel("s [m]",fontsize = 14)
    plt.ylabel(r"$\sigma(s)$ [mm]",fontsize = 14)
    plt.xlim(slong[0],slong[len(slong)-1])
    plt.legend(loc = 1,fontsize = 12)
    
    i = referenz[1][0]
    
    Kx,Ky,Kz = calculate_K(sigmax1,sigmay1,sigmaz1,i)
    Omp,Omm,omz = calculate_D(Kx,Ky,Kz)
    
    plt.figure(2)
    
    plt.plot(S,Omp,label=r"$\Omega_+$".format(n),color="orange")
    plt.plot(S,Omm,label=r"$\Omega_-$".format(n),color="blue")
    plt.plot(S,omz,label=r"$\omega_z$".format(n),color="purple")
    plt.xlim(S[0],S[len(S)-1])
    plt.title("Eigenvalues for one sector ({}MeV, {}mA)".format(Energy,I),fontsize =16)
    plt.xlabel(r"s [$m$]",fontsize = 14)
    plt.ylabel(r"$i\Omega_+$,$i\Omega_-$,$i\omega_z$ [$m^{-1}$]",fontsize = 14)
    plt.legend(loc = 0,fontsize = 12)
    
            
def plotting_diff():
    
    sigmas = np.loadtxt("data{}MeV/SigmaPerAngleForEnergy2_{}MeV.dat".format(Energy,Energy),unpack=True)
    sigmaslow = np.loadtxt("data{}MeVlow/SigmaPerAngleForEnergy2_{}MeV.dat".format(Energy,Energy),unpack=True)
    #sigmaxr1 = np.sqrt(referenz[0][7*2+1])*10**-3
    #sigmayr1 = np.sqrt(referenz[2][7*2+3])*10**-3
    #sigmazr1 = np.sqrt(referenz[4][7*2+5])*10**-3
    
    sigmax = []
    sigmay = []
    sigmaz = []
    
    sigmax1 = []
    sigmay1 = []
    sigmaz1 = []
    
    l = int(len(sigmas[0])/6)
   
    for i in range(l):
        
        sigmax.append(np.sqrt(np.abs(sigmas[0][i*6]))*10**-3)
        sigmay.append(np.sqrt(np.abs(sigmas[2][i*6+2]))*10**-3)
        sigmaz.append(np.sqrt(np.abs(sigmas[4][i*6+4]))*10**-3)
        
        sigmax1.append(np.sqrt(np.abs(sigmaslow[0][i*6]))*10**-3)
        sigmay1.append(np.sqrt(np.abs(sigmaslow[2][i*6+2]))*10**-3)
        sigmaz1.append(np.sqrt(np.abs(sigmaslow[4][i*6+4]))*10**-3)
        
     
    sigmax = np.array(sigmax)
    sigmay = np.array(sigmay)
    sigmaz = np.array(sigmaz)
    
    sigmax1 = np.array(sigmax1)
    sigmay1 = np.array(sigmay1)
    sigmaz1 = np.array(sigmaz1)
    
    S = s[:-876]
    
    plt.figure(1)
    for i in range(8):
        if(i == 7):
            plt.plot(S+i*(S[len(S)-1]),(sigmax-sigmax1)*1000,label =r"$\sigma_{x}$",color="orange")
            #plt.plot(S+i*(S[len(S)-1]),(sigmay-sigmay1)*1000,label= r"$\sigma_{y}$",color="blue")
            plt.plot(S+i*(S[len(S)-1]),(-sigmaz+sigmaz1)*1000,label=r"$\sigma_{z}$",color="green")
        else:
            plt.plot(S+i*(S[len(S)-1]),(sigmax-sigmax1)*1000,color="orange")
            #plt.plot(S+i*(S[len(S)-1]),(sigmay-sigmay1)*1000,color="blue")
            plt.plot(S+i*(S[len(S)-1]),(-sigmaz+sigmaz1)*1000,color="green")
        
    plt.title(r"Beam size difference for one turn ({}MeV)".format(Energy,I),fontsize = 16)
    plt.xlabel("s [m]",fontsize = 14)
    plt.ylabel(r"$\Delta\sigma(s)$ [mm]",fontsize = 14)
    plt.legend(loc = 0,fontsize = 12)
    #plt.yscale("log")
    
    #Kx,Ky,Kz = calculate_K(sigmax1,sigmay1,sigmaz1)
    #calculate_D(Kx,Ky,Kz)
    
def plotting_wrong():
    
    ls = {0 : "--", 1 : "-.", 2 : "-", 3 : ":"}
    i = referenz[1][0]
    
    for n in range(4):
        sigmas = np.loadtxt("data{}MeV/SigmaPerAngleForEnergy{}_{}MeV.dat".format(Energy,n,Energy),unpack=True)
        
        sigmax1 = []
        sigmay1 = []
        sigmaz1 = []
    
        l = int(len(sigmas[0])/6)
   
        for i in range(l):
        
            sigmax1.append(np.sqrt(np.abs(sigmas[0][i*6]))*10**-3)
            sigmay1.append(np.sqrt(np.abs(sigmas[2][i*6+2]))*10**-3)
            sigmaz1.append(np.sqrt(np.abs(sigmas[4][i*6+4]))*10**-3)
        
        sigmax1 = np.array(sigmax1)
        sigmay1 = np.array(sigmay1)
        sigmaz1 = np.array(sigmaz1)
    
        S = s[:-876]
        
        plt.figure(1)
    
        plt.plot(S,sigmax1*1000,label =r"{}:$\sigma_x$".format(n),color="orange",linestyle = ls[n])
        plt.plot(S,sigmay1*1000,label= r"{}:$\sigma_z$".format(n),color="blue",linestyle = ls[n])
        plt.plot(S,sigmaz1*1000,label=r"{}:$\sigma_l$".format(n),color="purple",linestyle= ls[n])
        
        if(n==3):
            
            plt.title(r"Beam size for one sector ({}MeV , {}mA)".format(Energy,I),fontsize = 16)
            plt.xlabel("s [m]",fontsize = 14)
            plt.ylabel(r"$\sigma(s)$ [mm]",fontsize = 14)
            plt.legend(loc = 1,fontsize = 12)   
            plt.xlim(S[0],S[len(S)-1]) 
        
        plt.figure(2)
        
        Kx,Ky,Kz = calculate_K(sigmax1,sigmay1,sigmaz1,i)
        Omp,Omm,omz= calculate_D(Kx,Ky,Kz)
        
        plt.plot(S,Omp,label=r"{}:$\Omega_+$".format(n),color="orange",linestyle = ls[n])
        plt.plot(S,Omm,label=r"{}:$\Omega_-$".format(n),color="blue",linestyle = ls[n])
        plt.plot(S,omz,label=r"{}:$\Omega_z$".format(n),color="purple",linestyle = ls[n])
        
        if(n==3):
            plt.title(r"Eigenvalues for one sector({}MeV , {}mA)".format(Energy,I),fontsize = 16)
            plt.xlabel("s [m]",fontsize = 14)
            plt.ylabel(r"i$\Omega_{+}$,i$\Omega_{-}$,i$\omega_{z}$ [mm]",fontsize = 14)
            plt.legend(loc = 1,fontsize = 12)   
            plt.xlim(S[0],S[len(S)-1]) 
        
                    
    
#plotting_diff()    
plotingOmega()    
#plotting_wrong()
























